import React, { useState, useRef, useEffect } from "react";
import ConsoleInput from "./ConsoleInput";
import executeCommand from "../../utils/executeCommand";
import ConsoleDisplay from "./ConsoleDisplay";
import "./style.css";

const Console = () => {
  const [history, setHistory] = useState([]);

  const handleCommandSubmit = (command) => {
    const output = executeCommand(command, history);
    setHistory(output);
  };

  const endOfContentRef = useRef(null);

  useEffect(() => {
    if (endOfContentRef.current) {
      endOfContentRef.current.scrollIntoView({ behavior: "smooth" });
    }
  }, [history]);

  return (
    <div className="console-container">
      <div className="console-content">
        <ConsoleDisplay history={history} />
        <div ref={endOfContentRef} />
      </div>
      <div className="console-entry">
        <ConsoleInput onCommandSubmit={handleCommandSubmit} />
      </div>
    </div>
  );
};

export default Console;
