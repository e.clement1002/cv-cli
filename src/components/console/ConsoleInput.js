import React, { useState, useRef, useEffect } from "react";
import "./style.css";

// dans un composant React les parametres ou props sont toujours entourés de brackets accolades
const ConsoleInput = ({ onCommandSubmit }) => {
  const [inputValue, setInputValue] = useState("");
  const inputRef = useRef(null);

  // en javascript e est l'evenement catché par le navigateur
  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  };

  const handleKeyPress = (e) => {
    // adapt it for mobile devices
    if (e.key === "Enter" || e.key === "Return") {
      // le return est utilisé pour le mobile ?
      onCommandSubmit(inputValue);
      setInputValue("");
    }
  };

  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
  }, []);

  return (
    <>
      <div onClick={() => inputRef.current && inputRef.current.focus()}>
        <span className="console-prefix">user@terminal:~$ </span>
        <input
          className="console-input"
          type="text"
          ref={inputRef}
          value={inputValue}
          onChange={handleInputChange}
          onKeyDown={handleKeyPress}
        />
      </div>
    </>
  );
};

export default ConsoleInput;
