import React from "react";
import DOMPurify from "dompurify";
import "./style.css";
import TypingEffect from "./TypingEffect";

const ConsoleDisplay = ({ history }) => {
  return (
    <>
      {history.map((entry, index) => {
        if (typeof entry === "object" && entry.__html) {
          // Utilisez une version modifiée de TypingEffect ou un nouveau composant si nécessaire
          // pour gérer le contenu HTML avec effet de frappe.
          return (
            <div key={index} className="console-line">
              <TypingEffect htmlContent={entry.__html} speed={15} />
            </div>
          );
        } else {
          // Pour les chaînes normales, continuez à utiliser TypingEffect comme avant.
          const sanitizedText = DOMPurify.sanitize(entry);
          return (
            <div className="console-line" key={index}>
              <TypingEffect text={sanitizedText} speed={15} />
            </div>
          );
        }
      })}
    </>
  );
};

export default ConsoleDisplay;
