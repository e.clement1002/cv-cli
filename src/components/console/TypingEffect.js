// TypingEffect.js
import React, { useState, useEffect } from 'react';
import DOMPurify from 'dompurify';

const TypingEffect = ({ htmlContent, speed }) => {
  const [display, setDisplay] = useState('');
  const [queue, setQueue] = useState('');

  useEffect(() => {
    setQueue(DOMPurify.sanitize(htmlContent)); // Assurez-vous que le HTML est sécurisé avant de le traiter
  }, [htmlContent]);

  useEffect(() => {
    if (!queue) return;

    const handle = window.setInterval(() => {
      setDisplay((currentDisplay) => {
        return queue.substring(0, currentDisplay.length + 1);
      });

      if (display.length === queue.length) {
        window.clearInterval(handle);
      }
    }, speed);

    return () => window.clearInterval(handle);
  }, [queue, display, speed]);

  return <div dangerouslySetInnerHTML={{ __html: display }} />;
};

export default TypingEffect;
