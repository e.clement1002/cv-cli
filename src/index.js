import React from 'react';
import ReactDOM from 'react-dom/client';
import Console from './components/console/Console';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
   <Console />
  </React.StrictMode>
);