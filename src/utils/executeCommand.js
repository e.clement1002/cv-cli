import commands from "../constants/commands";

const commandNotFoundObject = {
  __html: `<strong>Command not found</strong> <p>type <code>help</code> to see the list of available commands</p>`,
};

const executeCommand = (command, history) => {
  // on accede a un object par sa clé comme ceci commands[command]
  // la notation ...nom de mon object / de mon tableau est appelé spread operator
  if (commands[command]) {
    return [...history, `> ${command}`, commands[command]()];
  } else {
    return [...history, `> ${command}`, commandNotFoundObject];
  }
};

export default executeCommand;
