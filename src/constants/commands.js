// commands.js

const jobs = `
<p> Liste des emplois:</p>
<table>
<tr>
<th>Poste</th>
<th>Entreprise</th>
<th>Durée</th>
</tr>
<tr>
<td>Technicien informatique</td>
<td>Le Plessis Informatique</td>
<td>2020</td>
</tr>
</table>
`;

const studies = `
<p> Liste des études:</p>
<table>
<tr>
<th>Diplôme</th>
<th>Etablissement</th>
<th>Durée</th>
</tr>
<tr>
<td>Médecine</td>
<td>Université de Plovdiv</td>
<td>2020-today</td>
</tr>
<tr>
<td>Bac S</td>
<td>Paris</td>
<td>2019</td>
</tr>
<tr>
</table>
`;

const me = `
<p>
___________.__             .___.__                
\_   _____/|  |   ____   __| _/|__| ____          
 |    __)_ |  |  /  _ \ / __ | |  |/ __ \         
 |        \|  |_(  <_> ) /_/ | |  \  ___/         
/_______  /|____/\____/\____ | |__|\___  >        
        \/                  \/         \/         
_________ .__                                __   
\_   ___ \|  |   ____   _____   ____   _____/  |_ 
/    \  \/|  | _/ __ \ /     \_/ __ \ /    \   __\
\     \___|  |_\  ___/|  Y Y  \  ___/|   |  \  |  
 \______  /____/\___  >__|_|  /\___  >___|  /__|  
        \/          \/      \/     \/     \/      
</p>
`;

const commandWithoutHelp = {
  jobs: () => ({ __html: jobs }),
  studies: () => ({ __html: studies }),
  resume: () => ({ __html: `<a href="/cv.pdf" target="_blank">CV</a>` }),
  me: () => ({ __html: me }),
};

const generatedHelpList = Object.keys(commandWithoutHelp)
  .map((command) => `<li><code>${command}</code></li>`)
  .join("");

const commands = {
  ...commandWithoutHelp,
  help: () => ({ __html: generatedHelpList }),
};

export default commands;
